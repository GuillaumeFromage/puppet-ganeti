# @summary Setup files and other resources for a ganeti node
#
# @api private
#
class ganeti::config {

  assert_private()

  if $::kernelmajversion !~ /^2\..*/ {
    kmod::load { 'vhost_net' : }
  }

  if $ganeti::security_model == 'pool' {
    Integer[0, $ganeti::pool_num_users - 1].each |Integer $increment| {
      $uid = $ganeti::pool_uid_base + $increment

      user { "${ganeti::pool_username_prefix}${increment}":
        ensure  => present,
        uid     => $uid,
        comment => 'Ganeti VM pool user',
        home    => '/dev/null',
        shell   => '/usr/sbin/nologin'
      }
    }
  }

  if $ganeti::use_debootstrap_os_template {
    file { '/etc/ganeti/instance-debootstrap':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0750',
      purge   => $ganeti::purge_instance_debootstrap,
      recurse => true,
      source  => 'puppet:///modules/ganeti/instance-debootstrap',
    }

    file { '/etc/ganeti/instance-debootstrap/variants.list':
      ensure  => present,
      owner   => 'root',
      group   => 0,
      mode    => '0640',
      content => epp(
        'ganeti/instance-debootstrap-variants',
        {variants => $ganeti::debootstrap_variants}
      ),
    }
    $ganeti::debootstrap_variants.each |String $variant_name, Ganeti::Variant $variant| {
      file { "/etc/ganeti/instance-debootstrap/variants/${variant_name}.conf":
        ensure  => present,
        owner   => 'root',
        group   => 0,
        mode    => '0640',
        content => epp(
          'ganeti/instance-debootstrap-variant-config',
          {variant => $variant}
        ),
      }
    }

    if $ganeti::debootstrap_locale {
      file { '/etc/ganeti/instance-debootstrap/hooks/locale':
        ensure  => 'present',
        owner   => 'root',
        group   => 0,
        mode    => '0750',
        content => template('ganeti/instance-debootstrap-hooks-locale.erb'),
      }
    }
    if $ganeti::debootstrap_puppet_hook {
      file { '/etc/ganeti/instance-debootstrap/hooks/puppet':
        ensure  => present,
        owner   => 'root',
        group   => 0,
        mode    => '0750',
        content => template('ganeti/instance-debootstrap-hooks-puppet.erb'),
      }
    }

    file { '/etc/default/ganeti-instance-debootstrap':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => epp('ganeti/instance-debootstrap-default.epp', {
        defaults => $ganeti::debootstrap_default_values,
        }),
    }
  }

  file { '/etc/cron.d/ganeti':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp($ganeti::cron_template);
  }

  if $ganeti::private_ip =~ Undef {
    warning(@(EOWARNING)
    $ganeti::private_ip was not set.
    The module will default to listening on all interfaces.
    It is strongly recommended that you configure the private IP in order to
    bind ganeti services to a non-public network.

    If you cannot have a second, more private network, you can set this
    parameter to any IP to silence this warning.
    |- EOWARNING
    )
  }

  file { '/etc/default/ganeti':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('ganeti/ganeti_default.epp',
      { private_ip => $ganeti::private_ip }
    ),
  }

  # During initial install of ganeti, it's possible that this directory
  # doesn't exist and the next file will fail to be created. The directory is
  # probably created by the daemon once it's started since the file is created
  # correctly on the second run. To avoid useless errors we'll make sure the
  # directory exists.
  file { '/var/lib/ganeti/rapi':
    ensure => directory,
    owner  => 'gnt-rapi',
    group  => 'gnt-masterd',
    mode   => '0750',
  }
  file { '/var/lib/ganeti/rapi/users':
    ensure  => present,
    owner   => 'gnt-rapi',
    group   => 'gnt-masterd',
    mode    => '0640',
    content => epp('ganeti/rapi_users.epp',
      { users => $ganeti::rapi_users }
    ),
  }

  # @TODO: this runs on every run.. it should be moved over to being a cronjob
  tidy { '/var/lib/ganeti/queue':
    age     => '4w',
    matches => ['job-*'],
    recurse => true,
  }

  if $ganeti::with_drbd {
    kmod::option { 'drbd minor_count':
      module => 'drbd',
      option => 'minor_count',
      value  => '128',
    }
    kmod::option { 'drbd usermode_helper':
      module => 'drbd',
      option => 'usermode_helper',
      value  => '/bin/true',
    }

    kmod::load { 'drbd': }

    Kmod::Option['drbd minor_count']
    -> Kmod::Option['drbd usermode_helper']
    -> Kmod::Load['drbd']
  }

}
